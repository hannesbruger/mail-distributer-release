# Mail Distributer - v 0.2 Beta
*(c) by Hannes Bruger, 2019*

### Windows only (until now)!
--------------------------------
* **Installation:**
    Not required, just unpack the zip-folder and get started (MailDistributer.exe)
* **Attention**:
    * Still in development! You will probably find some bugs. let me know, where those are I will try to fix them.
    * Also please tell me, if you'd like to have a certain feature or don't like a specific aspect.<br>
        I will try to change it.